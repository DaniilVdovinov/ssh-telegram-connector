from fastapi import FastAPI, Request
import uvicorn
from pydantic import BaseModel
from typing import Optional, List

import pandas as pd
from simpletransformers.seq2seq import Seq2SeqModel

app = FastAPI()

delimiter = '<SEP>'

model_args = {
    "reprocess_input_data": True,
    "overwrite_output_dir": True,
    "max_seq_length": 10,
    "train_batch_size": 2,
    "num_train_epochs": 10,
    "save_eval_checkpoints": False,
    "save_model_every_epoch": False,
    "evaluate_generated_text": True,
    "evaluate_during_training_verbose": True,
    "use_multiprocessing": False,
    "max_length": 15,
    "manual_seed": 4,
}

encoder_type = "roberta"
model = Seq2SeqModel(
    encoder_type,
    "roberta-base",
    "bert-base-cased",
    args=model_args,
    use_cuda=False,
)


class MetricStatus(BaseModel):
    type: str
    service: str
    isOk: bool


class ScenarioRequest(BaseModel):
    metrics: List[MetricStatus]
    serverId: int


class CommandMetrics(BaseModel):
    metrics: List[MetricStatus]
    commands: str


class SessionResultForm(BaseModel):
    serverId: int
    commands: List[CommandMetrics]


@app.get("/scenarios")
async def get_scenario_for_server(request: ScenarioRequest):
    sequence = str(request.serverId) + ' ' + transform_metrics_to_sequence(request.metrics)
    return model.predict(sequence).split(delimiter)


@app.post("/session-result")
async def handle_session_result(request: SessionResultForm):
    data = []
    for command in request.commands:
        sequence = str(request.serverId) + ' ' + transform_metrics_to_sequence(command.metrics)
        data.append([sequence, command.command])
    df = pd.DataFrame(data, columns=["input_text", "target_text"])
    model.train_model(df)
    return "OK"


def transform_metrics_to_sequence(metrics_statuses):
    sequence = ''
    for status in metrics_statuses:
        if status.service is not None:
            sequence += status.service + ' '
        sequence += status.service + ' '
        if status.isOk is True:
            sequence += 'OK '
        else:
            sequence += 'FAIL '
    return sequence


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=9876)
