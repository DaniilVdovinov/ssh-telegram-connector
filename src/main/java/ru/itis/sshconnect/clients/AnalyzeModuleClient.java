package ru.itis.sshconnect.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.itis.sshconnect.dto.analyze.SessionResultRequest;
import ru.itis.sshconnect.dto.analyze.ScenarioRequest;

import java.util.List;

@FeignClient(name = "analyze-client", url = "${analyze-module.url}")
public interface AnalyzeModuleClient {
    @GetMapping("/session-result")
    String sendExecutedCommand(@RequestBody SessionResultRequest form);

    @GetMapping("/scenarios")
    List<String> getScenarioForServer(@RequestBody ScenarioRequest form);
}
