package ru.itis.sshconnect.handlers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.itis.sshconnect.dto.UserMessage;
import ru.itis.sshconnect.models.User;
import ru.itis.sshconnect.service.ServersCommandService;

import static ru.itis.sshconnect.constants.CallbackCommands.*;

@Component
public class CommandMessageHandler {

    @Autowired
    private ServersCommandService commandService;

    public void handleCommandMessage(UserMessage message) {
        User user = message.getUser();
        String command = message.getText().substring("@".length());

        if (command.startsWith(CONNECT_TO_SERVER)) {
            commandService.connectToServer(user, command);
        } else if (command.startsWith(DELETE_SERVER)) {
            commandService.deleteServer(user, command);
        } else if (command.startsWith(SERVER_INFO)) {
            commandService.displayServerInfo(user, command);
        } else if (command.startsWith(SERVER_INFO_COMMAND)) {
            commandService.execInfoCommand(user, command);
        }
    }
}
