package ru.itis.sshconnect.handlers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.itis.sshconnect.dto.MessageDto;
import ru.itis.sshconnect.dto.UserMessage;
import ru.itis.sshconnect.models.Server;
import ru.itis.sshconnect.models.User;
import ru.itis.sshconnect.service.JwtService;
import ru.itis.sshconnect.service.ServersService;
import ru.itis.sshconnect.telegram.TelegramBot;
import ru.itis.sshconnect.utils.MessageFormatter;

import java.util.List;

import static ru.itis.sshconnect.constants.MenuCommand.*;

@Component
public class MenuMessageHandler {

    @Autowired
    private TelegramBot bot;

    @Autowired
    private ServersService serversService;
    @Autowired
    private JwtService jwtService;

    @Autowired
    private CommandMessageHandler commandMessageHandler;

    @Autowired
    private MessageFormatter messageFormatter;

    public void processMenuMessage(UserMessage message) {
        String messageText = message.getText().trim();
        User user = message.getUser();
        MessageDto resultMessage = null;

        if (messageText.startsWith(SERVERS_LIST)) {
            List<Server> serversOfUser = serversService.getServersOfUser(user);
            resultMessage = messageFormatter.formatServersList(serversOfUser);

        } else if (messageText.startsWith(HELP)) {
            resultMessage = MessageDto.from(messageFormatter.getHelpMessage());

        } else if (messageText.startsWith(ADD_SERVER)) {
            serversService.addServer(user, messageText);

        } else if (messageText.startsWith(SERVERS_INFO)) {
            List<Server> servers = serversService.getServersOfUser(user);
            resultMessage = messageFormatter.formatServersInfo(servers);

        } else if (messageText.startsWith(CONNECT_SERVER)) {
            List<Server> servers = serversService.getServersOfUser(user);
            resultMessage = messageFormatter.formatServersToConnect(servers);

        } else if (messageText.startsWith(REMOVE_SERVER)) {
            List<Server> servers = serversService.getServersOfUser(user);
            resultMessage = messageFormatter.formatServersToDelete(servers);

        } else if (messageText.startsWith(GENERATE_TOKEN)) {
            resultMessage = MessageDto.from(jwtService.generateToken(user));
            resultMessage.setEnableParseMode(false);

        } else if (messageText.startsWith("@")) {
            commandMessageHandler.handleCommandMessage(message);
        }
        if (resultMessage != null) {
            bot.sendMessage(resultMessage, user);
        }
    }

}
