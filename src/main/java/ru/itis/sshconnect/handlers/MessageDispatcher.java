package ru.itis.sshconnect.handlers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.itis.sshconnect.dto.UserMessage;
import ru.itis.sshconnect.models.User;
import ru.itis.sshconnect.service.UserService;

@Component
public class MessageDispatcher implements TelegramMessageHandler {

    @Autowired
    private MenuMessageHandler menuMessageHandler;
    @Autowired
    private ShellMessageHandler shellMessageHandler;
    @Autowired
    private UserService userService;

    @Override
    @Transactional
    public void handleUpdate(Update update) {
        User user = userService.getUserFromUpdate(update);
        UserMessage message = UserMessage.from(update, user);
        if (user.inMenu()) {
            menuMessageHandler.processMenuMessage(message);
        } else if (user.inShell()) {
            shellMessageHandler.processShellCommand(message);
        }
    }
}
