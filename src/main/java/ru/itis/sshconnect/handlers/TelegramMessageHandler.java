package ru.itis.sshconnect.handlers;

import org.telegram.telegrambots.meta.api.objects.Update;

public interface TelegramMessageHandler {
    void handleUpdate(Update update);
}
