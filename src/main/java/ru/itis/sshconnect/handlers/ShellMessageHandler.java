package ru.itis.sshconnect.handlers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.itis.sshconnect.dto.UserMessage;
import ru.itis.sshconnect.models.User;
import ru.itis.sshconnect.service.TelegramSshService;
import ru.itis.sshconnect.telegram.TelegramBot;

@Component
public class ShellMessageHandler {

    @Autowired
    private TelegramSshService sshService;
    @Autowired
    private TelegramBot bot;

    public void processShellCommand(UserMessage message) {
        String command = message.getText();
        User user = message.getUser();
        switch (command) {
            case "clear":
                sshService.clearTerminal(user);
                break;
            case "exit":
                sshService.exitSession(user);
                bot.sendMessage("Session closed", user);
                break;
            default:
                sshService.processShellCommand(message);
                break;
        }
        bot.deleteMessage(message);
    }
}
