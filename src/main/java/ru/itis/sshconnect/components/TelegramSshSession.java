package ru.itis.sshconnect.components;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelShell;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import ru.itis.sshconnect.models.Server;
import ru.itis.sshconnect.utils.Callback;
import ru.itis.sshconnect.utils.ShellOutUpdatedCallback;
import ru.itis.sshconnect.utils.ShellOutputHandler;

import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.UUID;

@Setter
@Slf4j
public class TelegramSshSession {

    @Getter
    private Server server;
    private Session session;
    private ChannelShell channel;
    @Getter
    private String sessionId;
    @Getter
    private long failedMetricsOnStartCount = 0;

    private boolean isOpen = false;

    private volatile String lastCommand = "";

    private PipedInputStream inputStream = new PipedInputStream();
    private PipedOutputStream outputStream = new PipedOutputStream();

    private TerminalUpdater updater;
    private ShellOutputHandler outputHandler;
    private int lastTextHash = 0;

    private Callback closeSessionCallback;
    private ShellOutUpdatedCallback onShellOutUpdated;

    private TelegramSshSession() {
    }

    @SneakyThrows
    public static TelegramSshSession openSession(Server server) {
        TelegramSshSession tgSession = new TelegramSshSession();
        JSch jsch = new JSch();

        Session session = jsch.getSession(server.getUsername(), server.getHost(), server.getPort());
        session.setPassword(server.getPassword());
        session.setConfig("StrictHostKeyChecking", "no");
        session.connect();

        Channel channel = session.openChannel("shell");
        channel.setInputStream(new PipedInputStream(tgSession.outputStream));
        channel.setOutputStream(new PipedOutputStream(tgSession.inputStream));
        channel.connect();

        tgSession.outputHandler = new ShellOutputHandler();
        tgSession.isOpen = true;
        tgSession.setSession(session);
        tgSession.setChannel((ChannelShell) channel);
        tgSession.startUpdater();
        tgSession.setServer(server);
        tgSession.setSessionId(UUID.randomUUID().toString());
        tgSession.setFailedMetricsOnStartCount(server.countFailedMetrics());

        return tgSession;
    }

    @SneakyThrows
    public synchronized void updateMessage() {
        String text = outputHandler.getText();
        int hashCode = text.hashCode();
        if (lastTextHash != hashCode) {
            onShellOutUpdated.callback(text, lastCommand);
            lastTextHash = hashCode;
        }
    }

    private void startUpdater() {
        updater = new TerminalUpdater();
    }

    public void closeSession() {
        if (isOpen) {
            updater.exit();
            channel.disconnect();
            session.disconnect();
            if (closeSessionCallback != null) {
                closeSessionCallback.invoke();
            }
        }
    }

    @SneakyThrows
    public void sendCommand(String cmd) {
        lastCommand = cmd;
        outputStream.write((cmd + "\n").getBytes(StandardCharsets.UTF_8));
    }

    @SneakyThrows
    public void sendSignal(String signal) {
        channel.sendSignal(signal);
    }

    public void clearTerminal() {
        if (isOpen) {
            outputHandler.clear();
            sendCommand("clear");
        }
    }

    public class TerminalUpdater extends Thread {
        public TerminalUpdater() {
            this.start();
        }

        boolean exit = false;

        public void exit() {
            exit = true;
        }

        @Override
        @SneakyThrows
        public void run() {
            while (!channel.isClosed() || exit) {
                if (inputStream.available() > 0) {
                    StringBuilder result = new StringBuilder();
                    byte[] tmp = new byte[1024];
                    while (inputStream.available() > 0) {
                        int i = inputStream.read(tmp, 0, 1024);
                        if (i < 0) break;
                        result.append(new String(tmp, 0, i));
                    }

                    if (!result.toString().isEmpty()) {
                        outputHandler.addText(result.toString());
                        updateMessage();
                    }
                }
                Thread.sleep(50);
            }
            if (channel.isClosed()) {
                closeSession();
                System.out.println("exit-status: " + channel.getExitStatus());
            }
        }
    }
}
