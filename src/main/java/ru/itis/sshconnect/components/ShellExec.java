package ru.itis.sshconnect.components;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import lombok.Getter;
import ru.itis.sshconnect.models.Server;

import java.io.InputStream;

public class ShellExec {

    private final Server server;
    private final String command;

    @Getter
    private String result;

    public ShellExec(Server server, String command) {
        this.server = server;
        this.command = command;
    }

    public ShellExec exec() {
        try {
            JSch jsch = new JSch();

            Session session = jsch.getSession(server.getUsername(), server.getHost(), server.getPort());
            session.setPassword(server.getPassword());
            session.setConfig("StrictHostKeyChecking", "no");
            session.connect();

            Channel channel = session.openChannel("exec");
            ((ChannelExec) channel).setCommand(command);

            InputStream in = channel.getInputStream();

            channel.connect();

            byte[] tmp = new byte[1024];
            StringBuilder result = new StringBuilder();
            while (true) {
                while (in.available() > 0) {
                    int i = in.read(tmp, 0, 1024);
                    if (i < 0) break;
                    result.append(new String(tmp, 0, i));
                }
                if (channel.isClosed()) {
                    if (in.available() > 0) continue;
                    break;
                }
                try {
                    Thread.sleep(50);
                } catch (Exception ignored) {
                }
            }
            this.result = result.toString();
            channel.disconnect();
            session.disconnect();
        } catch (Exception e) {
            System.out.println(e);
        }
        return this;
    }


}
