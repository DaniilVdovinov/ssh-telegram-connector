package ru.itis.sshconnect.dto;

import lombok.Builder;
import lombok.Data;
import lombok.Setter;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;

@Data
@Builder
public class MessageDto {
    private String text;
    private ReplyKeyboard replyKeyboard;
    @Setter
    @Builder.Default
    private boolean enableParseMode = true;

    public static MessageDto from(String text) {
        return MessageDto.builder()
                .text(text)
                .build();
    }
}
