package ru.itis.sshconnect.dto.api;

import lombok.Data;
import ru.itis.sshconnect.models.Server;

@Data
public class ServerForm {
    private String host;
    private String username;
    private Integer port = 22;
    private String password;

    public Server toModel() {
        return Server.builder()
                .host(host)
                .username(username)
                .port(port)
                .password(password)
                .build();
    }
}
