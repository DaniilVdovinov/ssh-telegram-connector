package ru.itis.sshconnect.dto.api;

import lombok.Builder;
import lombok.Data;
import ru.itis.sshconnect.models.Server;

@Data
@Builder
public class ServerDto {
    private Long id;
    private String host;
    private String username;
    private Integer port;
    private Boolean isOk;

    public static ServerDto from(Server server) {
        return ServerDto.builder()
                .id(server.getId())
                .host(server.getHost())
                .username(server.getUsername())
                .port(server.getPort())
                .isOk(server.getIsOk())
                .build();
    }
}
