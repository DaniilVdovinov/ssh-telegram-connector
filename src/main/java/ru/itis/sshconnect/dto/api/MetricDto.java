package ru.itis.sshconnect.dto.api;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;
import ru.itis.sshconnect.models.ServerMetric;
import ru.itis.sshconnect.models.enums.ServerMetricStatus;
import ru.itis.sshconnect.models.enums.ServerMetricType;

@Data
@Builder
public class MetricDto {
    private Long id;
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ServerMetricType type;
    private String serviceName;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ServerMetricStatus status;
    private String lastUpdated;

    public ServerMetric toModel() {
        return ServerMetric.builder()
                .serviceName(serviceName)
                .type(type)
                .status(ServerMetricStatus.OK)
                .build();
    }

    public static MetricDto from(ServerMetric metric) {
        return MetricDto.builder()
                .id(metric.getId())
                .status(metric.getStatus())
                .serviceName(metric.getServiceName())
                .type(metric.getType())
                .lastUpdated(metric.getLastUpdated().toString())
                .build();
    }
}
