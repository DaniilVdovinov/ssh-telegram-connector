package ru.itis.sshconnect.dto.analyze;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class ScenarioRequest {
    private List<MetricStatusDto> metrics;
    private Long serverId;
}
