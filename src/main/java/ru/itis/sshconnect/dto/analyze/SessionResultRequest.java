package ru.itis.sshconnect.dto.analyze;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class SessionResultRequest {
    private Long serverId;
    private List<CommandMetricsDto> commands;
}
