package ru.itis.sshconnect.dto.analyze;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class CommandMetricsDto {
    private String commands;
    private List<MetricStatusDto> metrics;
}
