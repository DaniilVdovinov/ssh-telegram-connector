package ru.itis.sshconnect.dto.analyze;

import lombok.Builder;
import lombok.Data;
import ru.itis.sshconnect.models.ServerMetric;

import java.util.List;
import java.util.stream.Collectors;

@Data
@Builder
public class MetricStatusDto {
    private String type;
    private Boolean isOk;
    private String service;

    public static MetricStatusDto from(ServerMetric metric) {
        return MetricStatusDto.builder()
                .type(metric.getType().toString())
                .isOk(metric.isOk())
                .service(metric.getServiceName())
                .build();
    }

    public static List<MetricStatusDto> from(List<ServerMetric> metrics) {
        return metrics.stream()
                .map(MetricStatusDto::from)
                .collect(Collectors.toList());
    }

}
