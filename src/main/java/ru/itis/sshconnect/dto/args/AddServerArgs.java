package ru.itis.sshconnect.dto.args;

import com.beust.jcommander.Parameter;
import lombok.Data;

@Data
public class AddServerArgs {
    @Parameter(names = {"-h", "--host"})
    private String host;

    @Parameter(names = {"-u", "--user"})
    private String user;

    @Parameter(names = {"-P", "--port"})
    private Integer port = 22;

    @Parameter(names = {"-p", "--password"})
    private String password;
}
