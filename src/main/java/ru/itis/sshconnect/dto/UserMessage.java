package ru.itis.sshconnect.dto;

import lombok.Builder;
import lombok.Data;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.itis.sshconnect.models.User;

@Data
@Builder
public class UserMessage {

    private Update update;
    private String text;
    private User user;

    public static UserMessage from(Update update, User user) {
        String text;
        if (update.hasMessage()) {
            text = update.getMessage().getText();
        } else if (update.hasCallbackQuery()) {
            text = update.getCallbackQuery().getData();
        } else {
            throw new IllegalArgumentException("Unable to get message text from update");
        }
        return UserMessage.builder()
                .user(user)
                .update(update)
                .text(text)
                .build();
    }

}
