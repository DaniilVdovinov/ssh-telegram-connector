package ru.itis.sshconnect.constants;

public class MenuCommand {
    public static final String HELP = "/help";

    public static final String ADD_SERVER = "/addserver";
    public static final String REMOVE_SERVER = "/deleteserver";
    public static final String CONNECT_SERVER = "/connectserver";
    public static final String SERVERS_LIST = "/serverslist";
    public static final String SERVERS_INFO = "/serversinfo";
    public static final String GENERATE_TOKEN = "/generatetoken";

}
