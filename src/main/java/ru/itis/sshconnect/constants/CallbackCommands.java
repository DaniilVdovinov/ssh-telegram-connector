package ru.itis.sshconnect.constants;

public class CallbackCommands {
    public static final String CONNECT_TO_SERVER = "connect-server";
    public static final String DELETE_SERVER = "delete-server";
    public static final String SERVER_INFO = "server-info";
    public static final String SERVER_INFO_COMMAND = "info-command";
}
