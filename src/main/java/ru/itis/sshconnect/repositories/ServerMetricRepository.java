package ru.itis.sshconnect.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.itis.sshconnect.models.Server;
import ru.itis.sshconnect.models.ServerMetric;

import java.util.List;

@Repository
public interface ServerMetricRepository extends JpaRepository<ServerMetric, Long> {
    List<ServerMetric> findAllByServer(Server server);
}
