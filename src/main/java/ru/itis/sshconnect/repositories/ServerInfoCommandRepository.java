package ru.itis.sshconnect.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.itis.sshconnect.models.ServerInfoCommand;

import java.util.Optional;

@Repository
public interface ServerInfoCommandRepository extends JpaRepository<ServerInfoCommand, Long> {
    Optional<ServerInfoCommand> findByCommand(String command);
}