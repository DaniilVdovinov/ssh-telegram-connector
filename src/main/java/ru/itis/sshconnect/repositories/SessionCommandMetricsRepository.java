package ru.itis.sshconnect.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.itis.sshconnect.models.SessionCommandMetrics;

import java.util.List;

@Repository
public interface SessionCommandMetricsRepository extends JpaRepository<SessionCommandMetrics, Long> {
    List<SessionCommandMetrics> findAllBySessionIdOrderById(String sessionId);
}