package ru.itis.sshconnect.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.itis.sshconnect.models.Server;
import ru.itis.sshconnect.models.User;

import java.util.List;

@Repository
public interface ServerRepository extends JpaRepository<Server, Long> {
    List<Server> findAllByUser(User user);
}
