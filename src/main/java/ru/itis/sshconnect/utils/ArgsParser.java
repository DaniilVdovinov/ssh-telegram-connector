package ru.itis.sshconnect.utils;

import com.beust.jcommander.JCommander;

public class ArgsParser {

    public static <T> void parse(T object, String args) {
        JCommander.newBuilder()
                .addObject(object)
                .args(args.split(" "))
                .build();
    }
}
