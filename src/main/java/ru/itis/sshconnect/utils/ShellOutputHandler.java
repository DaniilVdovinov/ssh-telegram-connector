package ru.itis.sshconnect.utils;

import java.util.Arrays;

public class ShellOutputHandler {

    private int lastLineStartPos;

    private int length;

    private final char[] buffer;
    private int carriagePos;

    private boolean hasLastLineText = false;
    private String lastLineTest = "";

    public ShellOutputHandler() {
        lastLineStartPos = 0;
        carriagePos = 0;
        buffer = new char[10000];
    }

    public String getText() {
        char[] chars = Arrays.copyOfRange(buffer, 0, carriagePos);
        String string = new String(chars);
        return hasLastLineText ? string + lastLineTest : string;
    }

    public void addText(String newText) {
        newText = newText.replaceAll("\u001B\\[[;\\d]*m", "")
                .replaceAll("\r\n", "\n")
                .replaceAll("\r\r", "\r");
        for (char c : newText.toCharArray()) {
            // Перемещаем каретку в начало строки
            if (c == '\r') {
                if (carriagePos > lastLineStartPos) {
                    lastLineTest = new String(Arrays.copyOfRange(buffer, lastLineStartPos, carriagePos));
                    hasLastLineText = true;
                }
                carriagePos = lastLineStartPos;
                continue;
            }
            // Добавляем переход на новую строку
            else if (c == '\n') {
                lastLineStartPos = carriagePos + 1;
                buffer[carriagePos] = c;
            }
            // Добавляем символ к результату
            else {
                buffer[carriagePos] = c;
                hasLastLineText = false;
            }
            if (carriagePos > length) {
                length = carriagePos;
            }
            carriagePos++;
        }
        checkLength();
    }

    public void checkLength() {
        if (carriagePos >= 4000) {
            System.arraycopy(buffer, carriagePos - 4000, buffer, 0, 4096);
            carriagePos = 4000;
        }
    }

    public void clear() {
        carriagePos = 0;
    }
}
