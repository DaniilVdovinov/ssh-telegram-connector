package ru.itis.sshconnect.utils;

public interface ShellOutUpdatedCallback {
    void callback(String message, String lastCommand);
}
