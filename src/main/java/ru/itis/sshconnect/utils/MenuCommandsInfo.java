package ru.itis.sshconnect.utils;

import org.springframework.stereotype.Component;
import ru.itis.sshconnect.constants.MenuCommand;

@Component
public class MenuCommandsInfo {

    public String getMenuCommandInfo(String command) {
        switch (command) {
            case MenuCommand.ADD_SERVER:
                return "ADD SERVER";
            case MenuCommand.SERVERS_INFO:
                return "SERVERS_INFO";
            default:
                return "COMMAND INFO";
        }
    }
}
