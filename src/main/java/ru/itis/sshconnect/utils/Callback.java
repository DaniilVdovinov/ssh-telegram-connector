package ru.itis.sshconnect.utils;

public interface Callback {
    void invoke();
}
