package ru.itis.sshconnect.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import ru.itis.sshconnect.constants.CallbackCommands;
import ru.itis.sshconnect.dto.MessageDto;
import ru.itis.sshconnect.models.Server;
import ru.itis.sshconnect.models.ServerInfoCommand;
import ru.itis.sshconnect.models.ServerMetric;
import ru.itis.sshconnect.repositories.ServerInfoCommandRepository;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static ru.itis.sshconnect.constants.CallbackCommands.*;
import static ru.itis.sshconnect.constants.MenuCommand.*;

@Component
public class MessageFormatter {

    @Autowired
    private ServerInfoCommandRepository serverInfoCommandRepository;

    public MessageDto formatServersList(List<Server> servers) {
        StringBuilder builder = new StringBuilder();
        if (!servers.isEmpty()) {
            builder.append("Список серверов");
        } else {
            builder.append("Не добавлено ни одного сервера. Чтобы добавить сервер напишите " + ADD_SERVER);
        }
        ReplyKeyboard keyboard = getServersKeyboardWithCommand(servers, SERVER_INFO);
        return MessageDto.builder()
                .replyKeyboard(keyboard)
                .text(builder.toString())
                .build();
    }

    public MessageDto formatServersInfo(List<Server> servers) {
        StringBuilder builder = new StringBuilder();
        servers.forEach(server -> {
            builder.append(server).append("\n");

            server.getMetrics().forEach(metric -> {
                String metricLine = String.format("%s %s %s", metric.getType(), metric.getStatus(), metric.getLastUpdated().toString());
                builder.append(metricLine).append("\n");
            });

            builder.append("\n");
        });
        return MessageDto.from(builder.toString());
    }

    public MessageDto formatServerInfo(Server server) {
        String builder = server + "\n";
        InlineKeyboardMarkup keyboard = getServerInfoCommandsKeyboard(server);
        return MessageDto.builder()
                .text(builder)
                .replyKeyboard(keyboard)
                .build();
    }

    public MessageDto formatServersToConnect(List<Server> servers) {
        ReplyKeyboard keyboard = getServersKeyboardWithCommand(servers, CONNECT_TO_SERVER);
        return MessageDto.builder()
                .text("Выберите сервер для присоединения")
                .replyKeyboard(keyboard)
                .build();
    }

    public MessageDto formatServersToDelete(List<Server> servers) {
        ReplyKeyboard keyboard = getServersKeyboardWithCommand(servers, CallbackCommands.DELETE_SERVER);
        return MessageDto.builder()
                .text("Выберите сервер для удаления")
                .replyKeyboard(keyboard)
                .build();
    }

    public MessageDto formatFailedMetric(ServerMetric metric) {
        StringBuilder builder = new StringBuilder();
        builder.append("⚠️ Проблема на сервере ").append(metric.getServer().getHost()).append("\n");
        builder.append(String.format("%s %s %s", metric.getStatus(), metric.getType(), metric.getServiceName()));
        builder.append("\n\n").append("Подключиться к серверу:");

        return MessageDto.builder()
                .text(builder.toString())
                .replyKeyboard(getServersKeyboardWithCommand(Collections.singletonList(metric.getServer()), CONNECT_TO_SERVER))
                .enableParseMode(false)
                .build();
    }

    public MessageDto formatScenario(ServerMetric metric, List<String> scenario) {
        StringBuilder builder = new StringBuilder();
        Server server = metric.getServer();
        builder.append(String.format("Для сервера %s доступен сценарий\n", server.getHost()));
        builder.append("Команды которые будут выполнены: \n");
        for (String command : scenario) {
            builder.append(command).append("\n");
        }
        return MessageDto.builder()
                .text(builder.toString())
                .replyKeyboard(getScenarioCommandKeyboard(scenario))
                .enableParseMode(false)
                .build();
    }

    private ReplyKeyboard getServersKeyboardWithCommand(List<Server> servers, String callbackCommand) {
        List<List<InlineKeyboardButton>> serverButtons = servers.stream()
                .map(server -> List.of(InlineKeyboardButton.builder()
                        .callbackData("@" + callbackCommand + "/" + server.getId())
                        .text(server.toString())
                        .build()))
                .collect(Collectors.toList());

        return InlineKeyboardMarkup.builder()
                .keyboard(serverButtons)
                .build();
    }

    private InlineKeyboardMarkup getServerInfoCommandsKeyboard(Server server) {
        Long serverId = server.getId();
        List<ServerInfoCommand> commands = serverInfoCommandRepository.findAll();
        List<List<InlineKeyboardButton>> buttons = commands.stream()
                .map(command -> List.of(InlineKeyboardButton.builder()
                        .callbackData("@" + SERVER_INFO_COMMAND + "/" + serverId + "/" + command.getCommand())
                        .text(command.getName())
                        .build()))
                .collect(Collectors.toList());
        return InlineKeyboardMarkup.builder()
                .keyboard(buttons)
                .build();
    }

    private InlineKeyboardMarkup getScenarioCommandKeyboard(List<String> commands) {
        return InlineKeyboardMarkup.builder()
                .keyboard(
                        Collections.singletonList(
                                Collections.singletonList(InlineKeyboardButton.builder()
                                        .text("Выполнить")
                                        .build())
                        )
                )
                .build();
    }

    public String getHelpMessage() {
        return "Доступные команды:\n\n" +
                HELP + " - справка\n\n" +
                SERVERS_INFO + " - статус серверов\n" +
                CONNECT_SERVER + " - подключиться к серверу\n\n" +
                SERVERS_LIST + " - список серверов\n" +
                ADD_SERVER + " - добавить сервер\n" +
                REMOVE_SERVER + " - удалить сервер\n"
                ;
    }
}
