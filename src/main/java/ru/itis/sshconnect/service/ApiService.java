package ru.itis.sshconnect.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.itis.sshconnect.dto.api.MetricDto;
import ru.itis.sshconnect.dto.api.ServerDto;
import ru.itis.sshconnect.dto.api.ServerForm;
import ru.itis.sshconnect.models.Server;
import ru.itis.sshconnect.models.ServerMetric;
import ru.itis.sshconnect.models.User;
import ru.itis.sshconnect.repositories.ServerMetricRepository;
import ru.itis.sshconnect.repositories.ServerRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ApiService {

    @Autowired
    private JwtService jwtService;

    @Autowired
    private ServerRepository serverRepository;
    @Autowired
    private ServerMetricRepository metricRepository;

    public ServerDto addServer(String token, ServerForm serverForm) {
        User user = jwtService.extractUser(token);
        Server server = serverForm.toModel();
        server.setUser(user);
        serverRepository.save(server);
        return ServerDto.from(server);
    }

    public ServerDto getServer(String token, Long serverId) {
        User user = jwtService.extractUser(token);
        Server server = getServer(user, serverId);
        return ServerDto.from(server);
    }

    private Server getServer(User user, Long serverId) {
        Server server = serverRepository.findById(serverId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Server not found"));
        if (server.getUser().equals(user)) {
            return server;
        } else throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Not user's server");
    }

    public MetricDto addMetric(String token, Long serverId, MetricDto form) {
        User user = jwtService.extractUser(token);

        Server server = getServer(user, serverId);
        ServerMetric serverMetric = form.toModel();
        serverMetric.setServer(server);

        metricRepository.save(serverMetric);
        return MetricDto.from(serverMetric);
    }

    public List<ServerDto> getServers(String token) {
        User user = jwtService.extractUser(token);
        return serverRepository.findAllByUser(user).stream()
                .map(ServerDto::from)
                .collect(Collectors.toList());
    }

    public void deleteServer(String token, Long serverId) {
        User user = jwtService.extractUser(token);
        Server server = getServer(user, serverId);
        serverRepository.delete(server);
    }

    public List<MetricDto> getMetrics(String token, Long serverId) {
        User user = jwtService.extractUser(token);
        Server server = getServer(user, serverId);
        return server.getMetrics().stream()
                .map(MetricDto::from)
                .collect(Collectors.toList());
    }
}
