package ru.itis.sshconnect.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.sshconnect.dto.MessageDto;
import ru.itis.sshconnect.models.Server;
import ru.itis.sshconnect.models.ServerInfoCommand;
import ru.itis.sshconnect.models.User;
import ru.itis.sshconnect.models.enums.UserState;
import ru.itis.sshconnect.repositories.ServerInfoCommandRepository;
import ru.itis.sshconnect.repositories.UserRepository;
import ru.itis.sshconnect.telegram.TelegramBot;
import ru.itis.sshconnect.utils.MessageFormatter;

@Service
public class ServersCommandService {

    @Autowired
    private TelegramSshService sshService;
    @Autowired
    private ServersService serversService;

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ServerInfoCommandRepository commandRepository;

    @Autowired
    private TelegramBot bot;
    @Autowired
    private MessageFormatter messageFormatter;

    public void displayServerInfo(User user, String command) {
        Server server = extractServer(command);
        MessageDto message = messageFormatter.formatServerInfo(server);

        bot.sendMessage(message, user);
    }

    public void connectToServer(User user, String command) {
        Server server = extractServer(command);
        user.setState(UserState.SHELL);
        userRepository.save(user);

        sshService.startSession(user, server);
    }

    public void deleteServer(User user, String command) {
        Server server = extractServer(command);

        serversService.deleteServer(user, server);
    }

    public void execInfoCommand(User user, String command) {
        Server server = extractServer(command);
        String commandName = command.split("/")[2];
        ServerInfoCommand infoCommand = commandRepository.findByCommand(commandName)
                .orElseThrow(() -> new IllegalArgumentException("Unknown command"));
        String out = sshService.execSingleCommand(infoCommand.getExecCommand(), server);
        bot.sendMessage(out, user);
    }

    private Server extractServer(String command) {
        Long serverId = Long.parseLong(command.split("/")[1]);
        return serversService.getServer(serverId);
    }
}
