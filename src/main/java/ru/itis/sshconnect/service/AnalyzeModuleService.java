package ru.itis.sshconnect.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.sshconnect.clients.AnalyzeModuleClient;
import ru.itis.sshconnect.components.TelegramSshSession;
import ru.itis.sshconnect.dto.analyze.CommandMetricsDto;
import ru.itis.sshconnect.dto.analyze.MetricStatusDto;
import ru.itis.sshconnect.dto.analyze.ScenarioRequest;
import ru.itis.sshconnect.dto.analyze.SessionResultRequest;
import ru.itis.sshconnect.models.Server;
import ru.itis.sshconnect.models.SessionCommandMetrics;
import ru.itis.sshconnect.repositories.SessionCommandMetricsRepository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ExecutorService;

@Service
public class AnalyzeModuleService {

    public static final Long DELAY_AFTER_COMMAND_MS = 100L;

    @Autowired
    private AnalyzeModuleClient analyzeModuleClient;
    @Autowired
    private ServersStatusService serversStatusService;

    @Autowired
    private SessionCommandMetricsRepository sessionCommandMetricsRepository;

    @Autowired
    private ExecutorService executorService;

    public void processExecutedCommand(TelegramSshSession session, String command) {
        // Если сессия была открыта не для исправления, то не обрабатываем эти команды
        if (session.getFailedMetricsOnStartCount() == 0) {
            return;
        }
        Server server = session.getServer();
        executorService.submit(() -> {
            try {
                Thread.sleep(DELAY_AFTER_COMMAND_MS);
            } catch (InterruptedException ignored) {
            }

            List<MetricStatusDto> metricsStatuses = serversStatusService.updateAndGetServerMetricsStatuses(server);

            SessionCommandMetrics sessionCommand = SessionCommandMetrics.builder()
                    .command(command)
                    .server(server)
                    .sessionId(session.getSessionId())
                    .metrics(metricsStatuses)
                    .build();

            sessionCommandMetricsRepository.save(sessionCommand);
        });
    }

    public List<String> getScenarioForServer(Server server) {
        List<MetricStatusDto> metricStatuses = MetricStatusDto.from(server.getMetrics());
        metricStatuses.sort(Comparator.comparing(MetricStatusDto::getType));
        ScenarioRequest scenarioRequest = ScenarioRequest.builder()
                .serverId(server.getId())
                .metrics(metricStatuses)
                .build();
        return analyzeModuleClient.getScenarioForServer(scenarioRequest);
    }

    public void processExitSession(TelegramSshSession session) {
        List<MetricStatusDto> metricsStatuses = serversStatusService.updateAndGetServerMetricsStatuses(session.getServer());
        long failedMetricsOnExitCount = metricsStatuses.stream().filter(status -> !status.getIsOk()).count();
        // Если кол-во плохих метрик в конце больше или равно кол-ву в начале, то значит ничего не исправили
        if (session.getFailedMetricsOnStartCount() >= failedMetricsOnExitCount) {
            return;
        }
        List<SessionCommandMetrics> commands = sessionCommandMetricsRepository.findAllBySessionIdOrderById(session.getSessionId());

        List<CommandMetricsDto> commandMetrics = groupByMetrics(commands);

        SessionResultRequest request = SessionResultRequest.builder()
                .commands(commandMetrics)
                .serverId(session.getServer().getId())
                .build();

        analyzeModuleClient.sendExecutedCommand(request);
    }

    public List<CommandMetricsDto> groupByMetrics(List<SessionCommandMetrics> commandMetrics) {
        List<CommandMetricsDto> result = new ArrayList<>();
        List<String> commands = new ArrayList<>();
        for (int i = 0; i < commandMetrics.size() - 1; i++) {
            boolean metricsEquals = isStatusesEquals(commandMetrics.get(i).getMetrics(), commandMetrics.get(i + 1).getMetrics());
            commands.add(commandMetrics.get(i).getCommand());
            if (!metricsEquals) {
                commands.add(commandMetrics.get(i + 1).getCommand());
                CommandMetricsDto commandMetricsDto = CommandMetricsDto.builder()
                        .metrics(commandMetrics.get(i).getMetrics())
                        .commands(String.join("<SEP>", commands))
                        .build();
                result.add(commandMetricsDto);
                commands.clear();
                i++;
            }
        }
        return result;
    }

    private boolean isStatusesEquals(List<MetricStatusDto> list1, List<MetricStatusDto> list2) {
        if (list1.size() != list2.size()) return false;
        list1.sort(Comparator.comparing(MetricStatusDto::getType).thenComparing(MetricStatusDto::getService));
        list2.sort(Comparator.comparing(MetricStatusDto::getType).thenComparing(MetricStatusDto::getService));

        for (int i = 0; i < list1.size(); i++) {
            if (list1.get(i).getIsOk() != list2.get(i).getIsOk()) {
                return false;
            }
        }
        return true;
    }
}
