package ru.itis.sshconnect.service;

import com.beust.jcommander.JCommander;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.sshconnect.constants.MenuCommand;
import ru.itis.sshconnect.dto.args.AddServerArgs;
import ru.itis.sshconnect.models.Server;
import ru.itis.sshconnect.models.User;
import ru.itis.sshconnect.repositories.ServerRepository;
import ru.itis.sshconnect.telegram.TelegramBot;
import ru.itis.sshconnect.utils.ArgsParser;
import ru.itis.sshconnect.utils.MenuCommandsInfo;

import java.util.List;

@Service
public class ServersService {

    @Autowired
    private ServerRepository serverRepository;
    @Autowired
    private MenuCommandsInfo commandsInfo;
    @Autowired
    private TelegramBot bot;

    public List<Server> getServersOfUser(User user) {
        return user.getServers();
    }

    public void addServer(User user, String messageText) {
        String serverParams = messageText.substring(MenuCommand.ADD_SERVER.length()).trim();
        if (serverParams.isEmpty()) {
            bot.sendMessage(commandsInfo.getMenuCommandInfo(MenuCommand.ADD_SERVER), user);
            return;
        }

        AddServerArgs params = new AddServerArgs();
        ArgsParser.parse(params, serverParams);

        Server server = createServer(params);
        server.setUser(user);

        serverRepository.save(server);
        bot.sendMessage("Сервер успешно добавлен", user);
    }

    public Server getServer(Long serverId) {
        return serverRepository.findById(serverId)
                .orElseThrow(() -> new IllegalArgumentException("Server not found"));
    }

    private Server createServer(AddServerArgs params) {
        return Server.builder()
                .host(params.getHost())
                .password(params.getPassword())
                .port(params.getPort())
                .username(params.getUser())
                .build();
    }

    public void deleteServer(User user, Server server) {
        serverRepository.delete(server);
        bot.sendMessage("Сервер удален", user);
    }
}
