package ru.itis.sshconnect.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.sshconnect.dto.MessageDto;
import ru.itis.sshconnect.models.Server;
import ru.itis.sshconnect.models.ServerMetric;
import ru.itis.sshconnect.telegram.TelegramBot;
import ru.itis.sshconnect.utils.MessageFormatter;

import java.util.List;

@Service
public class NotificationService {

    @Autowired
    private TelegramBot bot;

    @Autowired
    private MessageFormatter messageFormatter;

    @Autowired
    private AnalyzeModuleService analyzeModuleService;

    public void notifyUserAboutFailMetric(ServerMetric metric) {
        Server server = metric.getServer();
        if (server.getUser().inShell()) return;
        MessageDto message = messageFormatter.formatFailedMetric(metric);
        bot.sendMessage(message, server.getUser());

        List<String> scenario = analyzeModuleService.getScenarioForServer(server);
        if (!scenario.isEmpty()) {
            MessageDto scenarioMessage = messageFormatter.formatScenario(metric, scenario);
            bot.sendMessage(scenarioMessage, server.getUser());
        }
    }
}
