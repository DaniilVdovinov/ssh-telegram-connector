package ru.itis.sshconnect.service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.Claim;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.itis.sshconnect.models.User;
import ru.itis.sshconnect.repositories.UserRepository;

import java.util.Map;

@Service
public class JwtService {

    private static final String USER_ID_PARAM = "CHAT_ID";

    @Value("${jwt.secret}")
    private String secret;

    @Autowired
    private UserRepository userRepository;

    public Map<String, Claim> verifyToken(String token) {
        try {
            Algorithm algorithm = Algorithm.HMAC256(secret);
            JWTVerifier verifier = JWT.require(algorithm)
                    .build();
            return verifier.verify(token).getClaims();
        } catch (JWTVerificationException exception){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Invalid token");
        }
    }

    public String generateToken(User user) {
        try {
            Algorithm algorithm = Algorithm.HMAC256(secret);
            return JWT.create()
                    .withClaim(USER_ID_PARAM, user.getChatId())
                    .sign(algorithm);
        } catch (JWTCreationException exception){
            throw new IllegalArgumentException(exception);
        }
    }

    public User extractUser(String token) {
        Map<String, Claim> claims = verifyToken(token);
        Claim chatId = claims.get(USER_ID_PARAM);
        return userRepository.findByChatId(chatId.asString())
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found"));
    }

}
