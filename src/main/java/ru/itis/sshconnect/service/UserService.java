package ru.itis.sshconnect.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.itis.sshconnect.models.User;
import ru.itis.sshconnect.models.enums.UserState;
import ru.itis.sshconnect.repositories.UserRepository;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    public User getUserFromUpdate(Update update) {
        Long chatId;
        if (update.hasMessage()) {
            chatId = update.getMessage().getChatId();
        } else if (update.hasCallbackQuery()) {
            chatId = update.getCallbackQuery().getMessage().getChatId();
        } else {
            throw new IllegalArgumentException("Unable to extract user from update");
        }
        return userRepository.findByChatId(chatId.toString())
                .orElseGet(() -> createUser(chatId));
    }

    public User createUser(Long chatId) {
        User user = User.builder()
                .chatId(chatId.toString())
                .state(UserState.MENU)
                .build();
        userRepository.save(user);
        return user;
    }
}
