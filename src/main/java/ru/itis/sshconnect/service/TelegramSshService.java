package ru.itis.sshconnect.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import ru.itis.sshconnect.components.ShellExec;
import ru.itis.sshconnect.components.TelegramSshSession;
import ru.itis.sshconnect.dto.MessageDto;
import ru.itis.sshconnect.dto.UserMessage;
import ru.itis.sshconnect.models.Server;
import ru.itis.sshconnect.models.User;
import ru.itis.sshconnect.models.enums.UserState;
import ru.itis.sshconnect.repositories.ServerRepository;
import ru.itis.sshconnect.repositories.UserRepository;
import ru.itis.sshconnect.telegram.TelegramBot;

import javax.annotation.Nullable;
import javax.annotation.PreDestroy;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class TelegramSshService {

    private final Map<User, TelegramSshSession> sessionPool = new HashMap<>();

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private TelegramBot bot;

    @Autowired
    private AnalyzeModuleService analyzeModuleService;

    @PreDestroy
    public void destroy() {
        sessionPool.forEach((user, telegramSshSession) -> telegramSshSession.closeSession());
    }

    public void processShellCommand(UserMessage message) {
        User user = message.getUser();
        String command = message.getText();

        if (!sessionPool.containsKey(user)) {
            user.setState(UserState.MENU);
            userRepository.save(user);
        } else {
            TelegramSshSession session = sessionPool.get(user);
            String signal = extractSignal(command);
            if (signal == null) {
                session.sendCommand(command);
                analyzeModuleService.processExecutedCommand(session, command);
            } else {
                session.sendSignal(signal);
            }
        }
    }

    public void clearTerminal(User user) {
        if (sessionPool.containsKey(user)) {
            sessionPool.get(user).clearTerminal();
        }
    }

    public void exitSession(User user) {
        if (sessionPool.containsKey(user)) {
            user.setState(UserState.MENU);
            userRepository.save(user);
            TelegramSshSession session = sessionPool.get(user);
            session.closeSession();
            sessionPool.remove(user);
            analyzeModuleService.processExitSession(session);
        }
    }

    public TelegramSshSession startSession(User user, Server server) {
        if (sessionPool.containsKey(user)) {
            sessionPool.get(user).closeSession();
        }
        Message shellMessage = bot.sendMessage("Starting terminal...", user);

        TelegramSshSession session = TelegramSshSession.openSession(server);
        session.setOnShellOutUpdated((newOut, lastCommand) -> {
            MessageDto messageDto = MessageDto.builder()
                    .text(newOut)
                    .replyKeyboard(getKeyboardForExecutedCommand(lastCommand))
                    .build();
            bot.updateMessage(messageDto, shellMessage.getMessageId(), user);
        });
        sessionPool.put(user, session);
        return session;
    }

    public String execSingleCommand(String command, Server server) {
        ShellExec exec = new ShellExec(server, command).exec();
        return exec.getResult().trim();
    }

    public String extractSignal(String command) {
        switch (command) {
            case "SIGINT":
                return "KILL";
            case "SIGQUIT":
                return "KILL";
            default:
                return null;
        }
    }

    @Nullable
    private InlineKeyboardMarkup getKeyboardForExecutedCommand(String command) {
        if (command == null || command.trim().isEmpty()) {
            return null;
        }
        InlineKeyboardButton retry = InlineKeyboardButton.builder()
                .callbackData(command)
                .text("Повторить")
                .build();
        InlineKeyboardButton interrupt = InlineKeyboardButton.builder()
                .callbackData("SIGINT")
                .text("Прервать")
                .build();
        InlineKeyboardButton exit = InlineKeyboardButton.builder()
                .callbackData("SIGQUIT")
                .text("Выйти")
                .build();
        return InlineKeyboardMarkup.builder()
                .keyboard(List.of(List.of(retry, interrupt, exit)))
                .build();
    }
}
