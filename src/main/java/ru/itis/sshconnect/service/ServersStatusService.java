package ru.itis.sshconnect.service;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.itis.sshconnect.dto.analyze.MetricStatusDto;
import ru.itis.sshconnect.models.Server;
import ru.itis.sshconnect.models.ServerMetric;
import ru.itis.sshconnect.models.enums.ServerMetricStatus;
import ru.itis.sshconnect.repositories.ServerMetricRepository;
import ru.itis.sshconnect.repositories.ServerRepository;

import java.net.InetAddress;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

@Service
public class ServersStatusService {

    @Autowired
    private TelegramSshService sshService;
    @Autowired
    private NotificationService notificationService;

    @Autowired
    private ServerRepository serverRepository;
    @Autowired
    private ServerMetricRepository metricRepository;

    @Transactional
    public void updateAllServersStatus() {
        List<Server> all = serverRepository.findAll();
        for (Server server : all) {
            updateServerMetrics(server);
        }
    }

    public void updateServerMetrics(Server server) {
        boolean isOk = true;
        for (ServerMetric metric : server.getMetrics()) {
            isOk = isOk && updateMetric(metric);
            if (!metric.isOk()) {
                notificationService.notifyUserAboutFailMetric(metric);
            }
        }
        server.setIsOk(isOk);
        serverRepository.save(server);
    }

    @Transactional
    public List<MetricStatusDto> updateAndGetServerMetricsStatuses(Server server) {
        return metricRepository.findAllByServer(server).stream()
                .map(metric -> {
                    boolean isOk = updateMetric(metric);
                    return MetricStatusDto.builder()
                            .type(metric.getType().name())
                            .isOk(isOk)
                            .service(metric.getServiceName())
                            .build();
                })
                .collect(Collectors.toList());
    }

    @Transactional
    public boolean updateMetric(ServerMetric metric) {
        Server server = metric.getServer();
        String serviceName = metric.getServiceName();
        boolean isOk;
        switch (metric.getType()) {
            case PING:
                isOk = pingServer(server);
                break;
            case DOCKER:
                isOk = isDockerContainerUp(server, serviceName);
                break;
            case LINUX:
                isOk = isLinuxServiceUp(server, serviceName);
                break;
            case FREE_SPACE:
                isOk = isDiskFree(server, serviceName);
                break;
            default:
                throw new IllegalArgumentException("Unknown type of metric");
        }
        metric.setLastUpdated(LocalDateTime.now());
        metric.setStatus(isOk ? ServerMetricStatus.OK : ServerMetricStatus.FAIL);
        metricRepository.save(metric);
        return isOk;
    }

    @SneakyThrows
    private boolean pingServer(Server server) {
        InetAddress address = InetAddress.getByName(server.getHost());
        return address.isReachable(5000);
    }

    private boolean isDockerContainerUp(Server server, String containerName) {
        String command = String.format("docker ps --filter \"name=%s\" --format \"{{.Status}}\"", containerName);
        String out = sshService.execSingleCommand(command, server);
        if (out.isEmpty()) {
            return false;
        }
        return out.toLowerCase().startsWith("up");
    }

    private boolean isLinuxServiceUp(Server server, String serviceName) {
        String command = String.format("systemctl is-active %s", serviceName);
        String out = sshService.execSingleCommand(command, server);
        if (out.isEmpty()) {
            return false;
        }
        return out.toLowerCase().startsWith("active");
    }

    private boolean isDiskFree(Server server, String disk) {
        String command = String.format("df -h %s --output=pcent", disk);
        String out = sshService.execSingleCommand(command, server);
        try {
            int usedPercent = new Scanner(out)
                    .useDelimiter("\\D+").nextInt();
            return usedPercent < 80;
        } catch (Exception e) {
            return false;
        }
    }
}
