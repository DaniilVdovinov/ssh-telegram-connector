package ru.itis.sshconnect.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.itis.sshconnect.dto.api.MetricDto;
import ru.itis.sshconnect.dto.api.ServerDto;
import ru.itis.sshconnect.dto.api.ServerForm;
import ru.itis.sshconnect.service.ApiService;

import java.util.List;

@RestController
@RequestMapping("/api")
public class ApiController {

    @Autowired
    private ApiService apiService;

    @PostMapping("/servers")
    public ResponseEntity<ServerDto> addServer(@RequestParam("token") String token, @RequestBody ServerForm serverForm) {
        return ResponseEntity.ok(apiService.addServer(token, serverForm));
    }

    @GetMapping("/servers")
    public ResponseEntity<List<ServerDto>> getServers(@RequestParam("token") String token) {
        return ResponseEntity.ok(apiService.getServers(token));
    }

    @GetMapping("/servers/{id}")
    public ResponseEntity<ServerDto> getServer(@RequestParam("token") String token, @PathVariable("id") Long serverId) {
        return ResponseEntity.ok(apiService.getServer(token, serverId));
    }

    @DeleteMapping("/servers/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteServer(@RequestParam("token") String token, @PathVariable("id") Long serverId) {
        apiService.deleteServer(token, serverId);
    }

    @PostMapping("/servers/{id}/metrics")
    public ResponseEntity<MetricDto> addServerMetric(@RequestParam("token") String token,
                                                     @PathVariable("id") Long serverId,
                                                     @RequestBody MetricDto form) {
        return ResponseEntity.ok(apiService.addMetric(token, serverId, form));
    }

    @GetMapping("/servers/{id}/metrics")
    public ResponseEntity<List<MetricDto>> getServerMetric(@RequestParam("token") String token,
                                                           @PathVariable("id") Long serverId) {
        return ResponseEntity.ok(apiService.getMetrics(token, serverId));
    }

}
