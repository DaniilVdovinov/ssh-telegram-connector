package ru.itis.sshconnect.schedulers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.itis.sshconnect.service.ServersStatusService;

@Component
public class MetricsScheduler {

    @Autowired
    private ServersStatusService service;

    @Scheduled(cron = "*/30 * * * * ?")
    public void updateAllServers() {
        service.updateAllServersStatus();
    }
}
