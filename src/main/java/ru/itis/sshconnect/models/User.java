package ru.itis.sshconnect.models;

import lombok.*;
import org.hibernate.Hibernate;
import ru.itis.sshconnect.models.enums.UserState;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "users")
@ToString(exclude = "servers")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private String chatId;

    @Enumerated(value = EnumType.STRING)
    @Column(columnDefinition = "varchar(50) default 'MENU'")
    private UserState state;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    private List<Server> servers;

    public boolean inMenu() {
        return this.state == UserState.MENU;
    }

    public boolean inShell() {
        return this.state == UserState.SHELL;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        User user = (User) o;

        return id != null && id.equals(user.id);
    }

    @Override
    public int hashCode() {
        return 1628885942;
    }
}
