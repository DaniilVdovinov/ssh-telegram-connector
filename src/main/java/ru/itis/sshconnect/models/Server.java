package ru.itis.sshconnect.models;


import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Server {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String host;
    private String username;
    private String password;
    private Integer port;

    private Boolean isOk;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "server", cascade = CascadeType.ALL)
    private List<ServerMetric> metrics;

    public long countFailedMetrics() {
        return this.metrics.stream()
                .filter(metric -> !metric.isOk()).count();
    }

    public String toString() {
        String status = this.isOk ? "✅" : "❌";
        return String.format("%s *%s* %s", status, host, username);
    }
}
