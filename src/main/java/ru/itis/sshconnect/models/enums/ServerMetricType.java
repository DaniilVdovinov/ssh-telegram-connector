package ru.itis.sshconnect.models.enums;

public enum ServerMetricType {
    PING, DOCKER, LINUX, FREE_SPACE
}
