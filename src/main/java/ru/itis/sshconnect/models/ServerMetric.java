package ru.itis.sshconnect.models;

import lombok.*;
import org.hibernate.Hibernate;
import ru.itis.sshconnect.dto.analyze.MetricStatusDto;
import ru.itis.sshconnect.models.enums.ServerMetricStatus;
import ru.itis.sshconnect.models.enums.ServerMetricType;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@ToString(exclude = "server")
public class ServerMetric {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    private ServerMetricType type;
    @Enumerated(EnumType.STRING)
    private ServerMetricStatus status;

    private String serviceName;

    private LocalDateTime lastUpdated;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "server_id")
    private Server server;

    public boolean isOk() {
        return this.status == ServerMetricStatus.OK;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        ServerMetric that = (ServerMetric) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
