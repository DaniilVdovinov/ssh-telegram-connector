package ru.itis.sshconnect.telegram;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.DeleteMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import ru.itis.sshconnect.configs.BotConfiguration;
import ru.itis.sshconnect.dto.MessageDto;
import ru.itis.sshconnect.dto.UserMessage;
import ru.itis.sshconnect.handlers.MessageDispatcher;
import ru.itis.sshconnect.models.User;

@Component
public class TelegramBot extends TelegramLongPollingBot {

    @Autowired
    private BotConfiguration botConfiguration;

    @Autowired
    private MessageDispatcher dispatcherMessageHandler;

    @Override
    public void onUpdateReceived(Update update) {
        dispatcherMessageHandler.handleUpdate(update);
    }

    @Override
    public String getBotUsername() {
        return botConfiguration.getName();
    }

    @Override
    public String getBotToken() {
        return botConfiguration.getToken();
    }

    @SneakyThrows
    public Message sendMessage(String text, User userTo) {
        return sendMessage(
                MessageDto.builder()
                        .text(text)
                        .build(),
                userTo);
    }

    @SneakyThrows
    public Message sendMessage(MessageDto messageDto, User userTo) {
        SendMessage sendMessage = SendMessage.builder()
                .text(messageDto.getText())
                .chatId(userTo.getChatId())
                .disableWebPagePreview(true)
                .build();
        if (messageDto.getReplyKeyboard() != null) {
            sendMessage.setReplyMarkup(messageDto.getReplyKeyboard());
        }
        if (messageDto.isEnableParseMode()) {
            sendMessage.setParseMode(ParseMode.MARKDOWN);
        }
        return execute(sendMessage);
    }

    @SneakyThrows
    public void updateMessage(MessageDto messageDto, Integer messageId, User user) {
        EditMessageText edit = EditMessageText.builder()
                .chatId(user.getChatId())
                .messageId(messageId)
                .text(messageDto.getText())
                .disableWebPagePreview(true)
                .build();
        if (messageDto.getReplyKeyboard() != null) {
            edit.setReplyMarkup((InlineKeyboardMarkup) messageDto.getReplyKeyboard());
        }
        try {
            execute(edit);
        } catch (Exception ignored) {
        }
    }

    @SneakyThrows
    public void deleteMessage(UserMessage message) {
        if (message.getUpdate().hasMessage()) {
            DeleteMessage deleteMessage = DeleteMessage.builder()
                    .chatId(message.getUser().getChatId())
                    .messageId(message.getUpdate().getMessage().getMessageId())
                    .build();
            execute(deleteMessage);
        }
    }
}
